;3.1
(define (make-accumulator val)
  (define v val)
  (lambda (n) (set! v (+ v n)) v)
)


(define A (make-accumulator 5))
(A 10)
(A 10)

;3.2
(define (make-monitored p)
  (define proc p)
  (define count 0)
  (lambda (msg)
    (if (equal? msg 'how-many-calls?) count (begin (set! count (+ count 1)) (proc msg)))
  )
)

(define s (make-monitored sqrt))
(s 100)

(s 'how-many-calls?)

;3.3
(define (make-account amount pass)
  (define $ amount)
  (define passwd pass)
  (define (inner-msg msg)
    (cond
      ((equal? msg 'withdraw) (lambda (n) (set! $ (- $ n)) $))
      ((equal? msg 'deposit) (lambda (n) (set! $ (+ $ n)) $))
    )
  )
  (define (check-pass pass msg)
    (cond
      ((equal? pass passwd) (inner-msg msg))
      (else (lambda (x) "Incorrect password"))
    )
  )
  (define (check-first pass msg)
    (cond
      ((equal? msg 'correct?) (if (equal? pass passwd) #t #f))
      (else (check-pass pass msg))
    )
  )
  check-first
)

(define acc (make-account 100 'secret-password))
((acc 'secret-password 'withdraw) 40)
((acc 'some-other-password 'deposit) 50)



;3.7
(define (make-joint account old-pass new-pass)
  (define old-account account)
  (define op old-pass)
  (define np new-pass)
  (define (check-pass pass msg)
    (cond
      ((or (equal? pass op) (equal? pass np)) (old-account op msg))
      (else (lambda (x) "Incorrect password"))
    )
  )
  (cond
    ((account old-pass 'correct?) check-pass)
    (else "Incorrect Original Password")
  )
)


(define peter-acc (make-account 100 'open-sesame))

(define paul-acc
  (make-joint peter-acc 'open-sesame 'rosebud))

((paul-acc 'rosebud 'withdraw) 40)
((paul-acc 'open-sesame 'deposit) 60)


;3.8
(define save '())

(define (f num)
  (set! save (append save (list num)))
  (car save)
)

(f 0)
(f 1)