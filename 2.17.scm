
;I'm using racket simple so I don't have to define a sort procedure
#lang racket

(require racket/trace)

;helpers
(define nil '())
(define (square x) (* x x))
(define (flatten list)
  (define (flatten-inner list flattened-part)
    (cond
      ((null? list) flattened-part)
      ((list? (car list)) (flatten-inner (append (car list) (cdr list)) flattened-part))
      (else (flatten-inner (cdr list) (cons (car list) flattened-part)))
      )
    )
  (reverse (flatten-inner list '()))
  )
(define (remove-all-from-list i l)
  (define (remove-all-from-list-i i l result)
    (cond
      ((null? l) result)
      ((not (equal? i (car l))) (remove-all-from-list-i i (cdr l) (cons (car l) result)))
      (else (remove-all-from-list-i i (cdr l) result))
    )
  )
  (remove-all-from-list-i i l '())
)

;(remove-all-from-list '() '(() 2 1))


;2.17


(define (last-pair l)
  (list-ref l (-(length l) 1))
  )

(last-pair (list 23 72 149 34))


;2.18

(define (reverse1 l)
  (define (reverse-inner l1 l2)
    (cond
      ((null? l1) l2)
      (else (reverse-inner (cdr l1) (cons (car l1) l2) ))
      )
    )
  (reverse-inner l '() )

  );(trace reverse1)

(reverse1 '(1 2 3))
(reverse1 (list 1 4 9 16 25))


;2.20
(define (same-parity 1st . rest)
  (define even (= (modulo 1st 2) 0))
  (define (same-parity-inner l1 l2)
    (cond
      ((null? l1) l2)
      (even (if (= (modulo (car l1) 2) 0) (same-parity-inner (cdr l1) (cons (car l1) l2)) (same-parity-inner (cdr l1) l2)))
      (else (if (not (= (modulo (car l1) 2) 0)) (same-parity-inner (cdr l1) (cons (car l1) l2)) (same-parity-inner (cdr l1) l2)))
      )
    )
  (cons 1st (reverse (same-parity-inner rest '())))
  )

(same-parity 1 2 3 4 5 6 7)
(same-parity 2 3 4 5 6 7)


;2.21
(define (square-list l)
  (define (square-list-inner l1 l2)
    (cond
      ((null? l1) l2)
      (else (square-list-inner (cdr l1) (cons (* (car l1) (car l1)) l2) ))
      )
    )
  (reverse1 (square-list-inner l '()))
  )

(square-list (list 1 2 3 4))


;2.22
(define (square-list1 items)
  (define (iter things answer)
    (if (null? things)
        answer
        (iter (cdr things)
              (cons (square (car things))
                    answer))))
  (iter items nil))

(square-list1 (list 1 2 3 4))
;because cons append to the last of the list

;the 2nd one doesn't work because chaning the order does not change how it is appened to a list, it is trying to append the list to the integer.


;2.23
;(for-each (lambda (x) (newline) (display x))
 ;         (list 57 321 88))

(for-each (lambda (x) (* x x)) '(11 2 3 ))

;2.25
(car(cdr(car(cdr(cdr'(1 3 (5 7) 9))))))

(car (car

'((7))))

(cadadr (cadadr (cadadr '(1 (2 (3 (4 (5 (6 7)))))))) )

;2.26
(define x (list 1 2 3))
(define y (list 4 5 6))

(append x y)
(cons x y)
(list x y)

;2.27
(define x1 (list (list 1 2) (list 3 4)))

x

(define (deep-reverse l)
  (define (deep-reverse-inner l1 sublist l2)
    (cond
      ((and (null? l1) (null? sublist)) l2)
      ((not (null? sublist)) (deep-reverse-inner l1 '() (cons (reverse sublist) l2)))
      ((list? (car l1)) (deep-reverse-inner (cdr l1) (car l1) l2) )
      (else (deep-reverse-inner (cdr l1) '() (cons (car l1) l2) ))
      )
    );(trace deep-reverse-inner)
  (deep-reverse-inner l '() '() )
  );(trace deep-reverse)

(reverse x1)
(deep-reverse x1)


;TFTN

(define (tftn)
  (define (find-nth-triangular n)
      (/(* n (+ n 1)) 2)
  )
  (define (find-all-factors n)
    (define (fafi n c l)
      (cond
        ((>= c n) l)
        (else (if (= (modulo n c) 0) (fafi n (+ c 1) (cons c l)) (fafi n (+ c 1) l)))
      )
     );(trace fafi)
    (fafi n 1 '()) 
   )
  ;(find-all-factors 10)
  (define (find-triplet n)
    (define list_of_factors (sort (find-all-factors n) <))
    (define result '())
    (for-each (lambda (1st) (for-each (lambda (2nd) (for-each (lambda (3rd) (if (= (* 1st 2nd 3rd) n) (set! result (cons (sort (list 1st 2nd 3rd) <) result )) (+ 1 1) )) list_of_factors)) list_of_factors)) list_of_factors)
    result
  )
  ;(find-triplet 6)
  (define (find-all-triangular-numbers-until n)
    (define (find-all-triangular-numbers-until-inner n c result)
      (cond
        ((>= c n) result)
        (else (find-all-triangular-numbers-until-inner n (+ c 1) (cons (find-nth-triangular c) result)))
      )
    )
    (reverse1 (find-all-triangular-numbers-until-inner n 1 '()))
  )
  ;(find-all-triangular-numbers-until 20)

  (define (triplet-exists n) (if (not (null? (find-triplet n))) #t #f))
  
  (define (filter-triplet l)
    (define (filter-triplets-inner l result)
      (cond
        ((null? l) result)
        ((and (= (- (cadr l) (car l)) 1) (= (- (caddr l) (cadr l)) 1)) (filter-triplets-inner '() (append l result)))
        (else (filter-triplets-inner '() '()))
      )
    )
    (filter-triplets-inner l '())
   )
  
  (define (tftn-inner)
    (define list_of_good_x '())
    (define result '())
    (define xs (find-all-triangular-numbers-until 22737))
    (for-each (lambda (x) (if (triplet-exists x) (set! list_of_good_x (cons x list_of_good_x)) '() )) xs)
    (for-each (lambda (x) (if (triplet-exists x) (set! result (cons (filter-triplet (car (find-triplet x))) result)) '())) list_of_good_x)
    (reverse (remove-all-from-list '() result))
  )
  
  ;(car (find-triplet 10))
  (tftn-inner)
)

(tftn)


