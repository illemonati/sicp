
(define nil '())
(define (square x) (* x x))

;2.17


(define (last-pair l)
  (list-ref l (-(length l) 1))
  )

(last-pair (list 23 72 149 34))


;2.18

(define (reverse1 l)
  (define (reverse-inner l1 l2)
    (cond
      ((null? l1) l2)
      (else (reverse-inner (cdr l1) (cons (car l1) l2) ))
      )
    )
  (reverse-inner l '() )
  
  );(trace reverse1)

(reverse1 '(1 2 3))
(reverse1 (list 1 4 9 16 25))


;2.20
(define (same-parity 1st . rest)
  (define even (= (modulo 1st 2) 0))
  (define (same-parity-inner l1 l2)
    (cond
      ((null? l1) l2)
      (even (if (= (modulo (car l1) 2) 0) (same-parity-inner (cdr l1) (cons (car l1) l2)) (same-parity-inner (cdr l1) l2)))
      (else (if (not (= (modulo (car l1) 2) 0)) (same-parity-inner (cdr l1) (cons (car l1) l2)) (same-parity-inner (cdr l1) l2)))
      )
    )
  (cons 1st (reverse (same-parity-inner rest '())))
  )

(same-parity 1 2 3 4 5 6 7)
(same-parity 2 3 4 5 6 7)


;2.21
(define (square-list l)
  (define (square-list-inner l1 l2)
    (cond
      ((null? l1) l2)
      (else (square-list-inner (cdr l1) (cons (* (car l1) (car l1)) l2) ))
      )
    )
  (reverse1 (square-list-inner l '()))
  )

(square-list (list 1 2 3 4))


;2.22
(define (square-list1 items)
  (define (iter things answer)
    (if (null? things)
        answer
        (iter (cdr things) 
              (cons (square (car things))
                    answer))))
  (iter items nil))

(square-list1 (list 1 2 3 4))
;because cons append to the last of the list

;the 2nd one doesn't work because chaning the order does not change how it is appened to a list, it is trying to append the list to the integer.


;2.23
;(for-each (lambda (x) (newline) (display x))
 ;         (list 57 321 88))

(for-each (lambda (x) (* x x)) '(11 2 3 ))

;2.25
(car(cdr(car(cdr(cdr'(1 3 (5 7) 9))))))

(car (car 

'((7))))

(cadadr (cadadr (cadadr '(1 (2 (3 (4 (5 (6 7)))))))) )

;2.26
(define x (list 1 2 3))
(define y (list 4 5 6))

(append x y)

(cons x y)

(list x y)