;2.1, 2.2, 2.4, 2.7, 2.8, 2.9

;global helpers
(define (start-problem n)
  (newline)
  (newline)
  (display '------------------)
  (newline)
  (display n)
  (newline)
  (display '------------------)
  (newline)
  )


;2.1

(start-problem 2.1)

(define (make-rat n d)
  (let ((g (gcd n d)))
    (cons (/ n g) (/ d g))))

(define (numer x) (car x))

(define (denom x) (cdr x))

(define (print-rat x)
  (newline)
  (display (
            numer x))
  (display "/")
  (display (denom x)))

(print-rat (make-rat 6 9))
(print-rat (make-rat -6 -9))
(print-rat (make-rat 6 -9))

(define (make-rat-better n d)
  (cond
    ((and (< n 0) (< d 0)) (make-rat (- 0 n) (- 0 d)))
    ((< (/ n d) 0) (make-rat (- 0 (abs n)) (abs d)))
    (else (make-rat n d))
    )
  )

(print-rat (make-rat-better 6 9))
(print-rat (make-rat-better -6 -9))
(print-rat (make-rat-better 6 -9))


;2.2
(start-problem 2.2)

(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

;point
(define (make-point x y)
  (cons x y)
  )
(define (x-point n)
  (car n)
  )
(define (y-point n)
  (cdr n)
  )

;segment
(define (make-segment p1 p2)
  (cons p1 p2)
  )
(define (start-segment segment)
  (car segment)
  )
(define (end-segment segment)
  (cdr segment)
  )

;midpoint
(define (midpoint-segment seg)
  (define find-x (/ (+ (x-point (start-segment seg)) (x-point (end-segment seg)) ) 2))
  (define find-y (/ (+ (y-point (start-segment seg)) (y-point (end-segment seg)) ) 2))
  (make-point find-x find-y)
  )


(define p1 (make-point 0 0))
(define p2 (make-point 10 10))
(print-point p1)
(print-point p2)
(define s1 (make-segment p1 p2))

(print-point (midpoint-segment s1))



;2.4
(start-problem 2.4)
(newline)

(define (cons1 x y)
  (lambda (m) (m x y)))

(define (car1 z)
  (z (lambda (p q) p)))

(car1 (cons1 1 2))
(car1 (cons1 2 1))

(define (cdr1 z)
  (z (lambda (p q) q)))

(cdr1 (cons1 1 2))
(cdr1 (cons1 2 1))

;2.7
(start-problem 2.7)
(newline)
(define (make-interval a b) (cons a b))

(define (upper-bound interval) (max (car interval) (cdr interval)))
(define (lower-bound interval) (min (car interval) (cdr interval)))

(define test-i (make-interval 3 1))
(upper-bound test-i)
(lower-bound test-i)

;2.8
; you subtract the lower bound of both, and the upper bound of both and make an interval with that
(start-problem 2.8)
(newline)
(define (sub-interval x y)
  (make-interval (- (lower-bound x) (lower-bound y))
                 (- (upper-bound x) (upper-bound y))))

(sub-interval test-i (make-interval 2 3))

;2.9
(start-problem 2.9)
(newline)

(define test-i2 (make-interval 7 8))
(define (width-interval i)
  (/ (- (upper-bound i) (lower-bound i)) 2))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (mul-interval x 
                (make-interval (/ 1.0 (upper-bound y))
                               (/ 1.0 (lower-bound y)))))

(width-interval test-i)

;add
(= (width-interval (add-interval test-i  test-i2)) (+ (width-interval test-i)  (width-interval test-i2)))

;divide
(= (width-interval (div-interval test-i  test-i2)) (/ (width-interval test-i)  (width-interval test-i2)))
